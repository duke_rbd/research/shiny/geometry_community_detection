get_Q <- function(n, p, d, snr, phi, theta) {

  U <- get_U(phi); Lambda <- get_Lambda(theta, snr); B <- get_B(p)
  W <- get_W(B, p); V <- get_V(U, W);
  
  Q_script <- get_Q_script(V, Lambda)
  Q_raw    <- (d*matrix(1, nrow = 3, ncol = 3) + sqrt(d)*Q_script)/n
  
  Q <- Q_raw
  
  # in case the off-diagonals are neglibly different
  Q[2,1] <- Q_raw[1,2]; Q[3,1] <- Q_raw[1,3]; Q[3,2] <- Q_raw[2,3] 
  
  return(Q)
  
}
