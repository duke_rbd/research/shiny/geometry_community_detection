# Mean squared deviation
get_MSD <- function(sample_means, cluster_means) {
  
  sample_means <- select(sample_means, -labels); cluster_means <- select(cluster_means, -labels)
  
  return(sum(sqrt(rowSums((sample_means - cluster_means)^2))))
  
}
