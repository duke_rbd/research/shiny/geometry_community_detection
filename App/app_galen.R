#### Libraries ####
library(tidyverse); library(igraph); library(pROC); library(combinat); library(MASS); library(expm)
library(gridExtra); library(latex2exp); library(scales); library(grid); library(shiny); library(shinydashboard); 
library(rsconnect); library(reshape); library(DT); library(plotly); library(R.utils)

sourceDirectory("boot", modifiedOnly = FALSE)

##give it U and lambda 1 and lambda 2
ui <- shinydashboard::dashboardPage(
  shinydashboard::dashboardHeader(title = "Exploring"),
  shinydashboard::dashboardSidebar(
    shinydashboard::sidebarMenu(     
      shinydashboard::menuItem("Multiple Community", tabName = "indep",icon = icon("users"))
      
    )
  ),
  shinydashboard::dashboardBody(
    shinydashboard::tabItems(
      shinydashboard::tabItem(tabName = "indep",
                              fluidRow(
                                
                                
                                box(
                                  
                                  title = "Inputs:", background = "black", width = 2, height = 800,
                                  
                                  
                                  selectInput("avgDeg_l", "Avg. Degree ",choices = c(
                                    "30" =30, "50" = 50, "100"=100)),
                                  
                                  # sliderInput("lambda_l", "Lambda = ", min = .2, max = 3, value = 1.4),
                                  # sliderInput("p_l", "p = ", min = 0, max = 1, value = .45) ),
                                  # selectInput( "p_l", "p = ", choices = c("0.495" = 0.495,
                                  #                                         "0.475" = 0.475,
                                  #                                         "0.450" = 0.450,
                                  #                                         "0.335" = 0.335,
                                  #                                         "0.250" = 0.250))),
                                  numericInput("p_1", "Prop. 1", value = 0.33),
                                  numericInput("p_2", "Prop. 2", value = 0.33),
                                  numericInput("p_3", "Prop. 3", value = 0.34),
                                  numericInput("snr", "Signal-to-noise ratio", value = 8),
                                  numericInput("phi", "Phi", value = 1.57),
                                  numericInput("theta", "Theta", value = 0.78),
                                 # selectInput("sub_deg", "Subtract degree", choices = c("Yes" = TRUE, "No" = FALSE)),
                                  selectInput("trans_BV", "Transform BV", choices = c("Yes" = TRUE, "No" = FALSE))),
                                
                                
                                box(width = 5, plotlyOutput("roheEigCompPlot3D"), height = 500, color = "maroon", title = ""),
                              
                                
                                box(width = 5, plotOutput("roheSimplePlot", height = 500)),
                                box(width = 3, plotOutput("vplot"), height = 500),
                                #box(plotOutput("ROC_curve"),width = 4, "ROC Curve"),
                                # box(width = 4, plotOutput("roheSimplePlot_transform"), height = 500, color = "maroon", title = "Transformed"),
                                
                                # box(plotOutput("splitPlot"),width = 4, "Optimal Split"),
                                
                                fluidRow(box(withMathJax(uiOutput("BMat")), width = 3,withMathJax(uiOutput("eigQn"))),
                                         box(withMathJax(uiOutput("QMat")), width = 3,withMathJax(uiOutput("eigQ"))),
                                         box(withMathJax(uiOutput("VMat")), width = 3),
                                         box(withMathJax(uiOutput("transMat")), width = 3,withMathJax(uiOutput("eigTrans")))),
                                box(width = 12, plotOutput("split_plot", height = 1200))
                                
                                
                                # box(withMathJax(uiOutput("lambdaVaL")))
                                
                              ))
    )))




server <- function(input, output) { 
  
  
  regimeCustomDat<- reactive({
    
    # Parse input
    n <- 1000; p <- c(input$p_1, input$p_2, input$p_3); d <- as.integer(input$avgDeg_l)
    blockSizes <- get_block_sizes(n, p)
    snr <- input$snr; theta <- input$theta; phi <- input$phi
    
    sub_deg <- FALSE
    trans_BV <- as.logical(input$trans_BV)
    
    
    # Calculate base statistics
    U <- get_U(phi); Lambda <- get_Lambda(theta, snr); B <- get_B(p)
    W <- get_W(B, p); V <- get_V(U, W);
    
    Q   <- get_Q_script(V, Lambda); eigQ <- eigen(Q)$values
    
    Q_n <- get_Q(n, p, d, snr, phi, theta); eigQn <- eigen(Q_n)$values
    
    # Get cluster means
    cluster_means <- as.data.frame(get_cluster_means(Q_n, blockSizes, B, sub_deg = sub_deg, trans_BV = trans_BV)) %>% 
      mutate(labels = as.factor(labels))
    
    ## expected centers when transformed
    expectedClusterMeans <-  p + sqrt(n)*B%*% ginv(B)%*%(diag(k)-p)/sqrt(n)
    if(expectedClusterMeans[1,1]<0){expectedClusterMeans <- -1*expectedClusterMeans}
    
    expectedClusterMeans <- as.data.frame(cbind(c(1,2,3),expectedClusterMeans))
    colnames(expectedClusterMeans) <- c("labels", "lambda_1", "lambda_2", "lambda_3")
    expectedClusterMeans$labels <- as.factor(expectedClusterMeans$labels)
    
    if(trans_BV == FALSE){
      expectedClusterMeans <- cluster_means
    }

    # Sample a graph
    adjMat <- graphGenerator(Q_n, blockSizes)
    #adjMat <- if(sub_deg) (adjMat - sum(adjMat)/n^2) else adjMat
    
    ## get eigen vectors
    eigs <- get_top3eig(adjMat)
    
    ## Consider sign change
    for (i in 1:3) {
      
      if(eigs[1,i] < 0) {
        eigs[, i] <- -eigs[, i]
      }
      
    }
    
    ## Consider transformations
    if (trans_BV) {
      
     # eigs <- if(sub_deg) eigs[,1:2] else eigs[,2:3]
      
      eigs <- p + t(sqrt(n)*B%*%t(eigs[,2:3]))
      # eigs <- t(B%*%t(eigs))
    }
    
    ## Get optimal rotation
    eigenExplore <- get_design_matrix(eigs, blockSizes)
    eigenExplore <- get_optimal_rot.and.lab(expectedClusterMeans, eigenExplore, sub_deg = sub_deg) %>% 
      mutate(labels = as.factor(labels))
    
   
    
    
    
    # Transition mat
    transMat <- n/d*(Q_n %*% diag(p))
    
    eigsTrans <- eigen(transMat)$values
    rohe_lambda <- eigsTrans[2]^2/eigsTrans[1]
    
    
    # Feedback
    print(c(n, p, d, snr, phi, theta))
    print(sprintf("Degree: %f", mean(rowSums(adjMat))))
    print(Q); print(Q_n); print(V); print("Qp:"); print(Q %*% p)
    
    print("Row sums of transition matrix")
    print(rowSums(transMat))
    
    list(eigenExplore = eigenExplore, adjMat = adjMat, Q_n = Q_n, cluster_means = cluster_means,
         lambda = rohe_lambda, Q = Q, V = V,transMat = transMat, eigQ = eigQ, eigQn = eigQn, eigsTrans= eigsTrans,
         blockSizes = blockSizes, sub_deg = sub_deg, trans_BV = trans_BV, B = B,expectedClusterMeans= expectedClusterMeans)
    
  })
  
  ## Q_n
  
  output$BMat<- renderUI({
    B <- regimeCustomDat()$Q_n
    
    
    withMathJax(sprintf("Q_n = \\begin{bmatrix} %.4f & %.4f & %.4f\\\\  %.4f & %.4f & %.4f\\\\ %.4f & %.4f & %.4f\\end{bmatrix}",
                        B[1,1], B[1,2], B[1,3],
                        B[2,1], B[2,2], B[2,3],
                        B[3,1], B[3,2], B[3,3]))
    
  })
  
  output$transMat<- renderUI({
    B <- regimeCustomDat()$transMat
    
    
    withMathJax(sprintf("TransitionMat = \\begin{bmatrix} %.4f & %.4f & %.4f\\\\  %.4f & %.4f & %.4f\\\\ %.4f & %.4f & %.4f\\end{bmatrix}",
                        B[1,1], B[1,2], B[1,3],
                        B[2,1], B[2,2], B[2,3],
                        B[3,1], B[3,2], B[3,3]))
    
  })
  
  
  output$eigTrans <- renderUI({
    B <- regimeCustomDat()$eigsTrans
    
    withMathJax(sprintf("Eigen(TransMat) = \\begin{bmatrix} %.4f & %.4f & %.4f\\end{bmatrix}",
                        B[1], B[2], B[3]
    ))
    
  })
  
  
  output$eigQ <- renderUI({
    B <- regimeCustomDat()$eigQ
    
    withMathJax(sprintf("Eigen(Q) = \\begin{bmatrix} %.4f & %.4f & %.4f\\end{bmatrix}",
                        B[1], B[2], B[3]
    ))
    
  })
  
  
  output$eigQn <- renderUI({
    B <- regimeCustomDat()$eigQn
    
    withMathJax(sprintf("Eigen(Q_n) = \\begin{bmatrix} %.4f & %.4f & %.4f\\end{bmatrix}",
                        B[1], B[2], B[3]
    ))
    
  })
  output$VMat<- renderUI({
    B <- regimeCustomDat()$V
    
    
    withMathJax(sprintf("V = \\begin{bmatrix} %.4f & %.4f & %.4f\\\\  %.4f & %.4f & %.4f\\end{bmatrix}",
                        B[1,1], B[1,2], B[1,3],
                        B[2,1], B[2,2], B[2,3]))
    
  })
  
  ## Q
  
  output$QMat<- renderUI({
    B <- regimeCustomDat()$Q
    
    withMathJax(sprintf("Q = \\begin{bmatrix} %.4f & %.4f & %.4f\\\\  %.4f & %.4f & %.4f\\\\ %.4f & %.4f & %.4f\\end{bmatrix}",
                        B[1,1], B[1,2], B[1,3],
                        B[2,1], B[2,2], B[2,3],
                        B[3,1], B[3,2], B[3,3]))
    
  })
  
  output$lambdaVal <- renderUI({
    Q <- regimeCustomDat()$Q
    values <- eigen(Q)$values
    withMathJax(sprintf("Eig(Q)_1 =  %.2f, Eig(Q)_2 = %.2f, Eig(Q)_3 = %.2f",values[1], values[2], values[3]))
    
  })
  
  output$split_plot <- renderPlot({
    Q_n <- regimeCustomDat()$Q_n
    blockSizes <- regimeCustomDat()$blockSizes
    B <- regimeCustomDat()$B
    sub_deg <- regimeCustomDat()$sub_deg; trans_BV <- regimeCustomDat()$trans_BV
    cluster_means <- get_cluster_means(Q_n, blockSizes, B, sub_deg = sub_deg, trans_BV = trans_BV)
    # get_classifiers(Q_n, blockSizes, cluster_means)
  })
  
  output$vplot <- renderPlot({
    V <- regimeCustomDat()$V
    plot_V(V)
    
  })
  
  output$roheEigCompPlot3D <- renderPlotly({
    
    eigenExplore <- regimeCustomDat()$eigenExplore
    cluster_means <- regimeCustomDat()$expectedClusterMeans
    
    p1 <- plot_ly(
      colors =c("cyan3", "orange3", "maroon"),
      type = "scatter3d"
      # marker = list(symbol = "circle", sizemode = 'diameter'),
      # scene = paste0("scene", sceneTeller)
    )%>%
      add_markers(x= ~eigenExplore[,1], y = ~eigenExplore[,2], z= ~eigenExplore[,3],
                  color = ~eigenExplore[,4],
                  # size = I(.001),
                  marker = list(size = 2, sizemode="area")) %>%
      add_markers(x = ~cluster_means$lambda_1, y = ~cluster_means$lambda_2, z = ~cluster_means$lambda_3,
                  marker = list(size = 8),
                  color = I("black"), showlegend=FALSE)
    
    p1
    
  })
  
  output$roheSimplePlot <- renderPlot({
    pastDat <- regimeCustomDat()
    
    adjacencyEigen <- pastDat$eigenExplore
   # cluster_means <- expectedClusterMeans;adjacencyEigen <- eigenExplore

    cluster_means <- pastDat$expectedClusterMeans
    
    #if(input$trans_BV==TRUE){li<-1.5}else{li<-1}
    
    g1 <- ggplot() +
      geom_point(data = adjacencyEigen, aes(adjacencyEigen[,2], adjacencyEigen[,1], col = labels)) +
      scale_color_manual(values =c("cyan3", "orange3", "maroon")) + guides(col = guide_legend("Group")) +
      geom_point(data = cluster_means,aes(x = cluster_means[,3], cluster_means[,2]), size = 5)+ scale_x_continuous(limits = c(-li,li), expand = c(0, 0)) + scale_y_continuous(limits = c(-li,li), expand = c(0, 0)) 
    
    
    
    g2 <- ggplot(adjacencyEigen) +
      geom_point(aes(adjacencyEigen[,3], adjacencyEigen[,1], col = labels)) +
      scale_color_manual(values =c("cyan3", "orange3", "maroon")) + guides(col = guide_legend("Group"))+
      geom_point(data = cluster_means,aes(x = cluster_means[,4], cluster_means[,2]), size = 5)
    
    g3 <- ggplot(adjacencyEigen) + 
      geom_point(aes(adjacencyEigen[,2], adjacencyEigen[,3], col = labels)) + 
      scale_color_manual(values =c("cyan3", "orange3", "maroon")) + guides(col = guide_legend("Group"))  +
      geom_point(data = cluster_means,aes(x = cluster_means[,3], cluster_means[,4]), size = 5)
    
    grid.arrange(g1,g2,g3, nrow = 3)
    
  })
  
}

shinyApp(ui, server)